#!/usr/bin/env python
import argparse
import dicom
import os
import sys 
import numpy
from natsort import natsorted

from matplotlib import pyplot, cm

def collect_files(path):
    dicom_files = []
    for dirpath, dirnames, filenames in os.walk(path):
        for fname in natsorted(filenames):
            dicom_files.append(os.path.join(dirpath, fname))
    return dicom_files

def filter_dicom(dicom_files):
    filtered_dicom_files = []
    dicom_series = {}

    for fname in dicom_files:
        try:
            dicom_file = dicom.read_file(fname)
            series = int(dicom_file.SeriesNumber)
            if series in dicom_series:
                dicom_series[series].append(fname)
            else:
                dicom_series[series] = [fname]
        except (TypeError, dicom.errors.InvalidDicomError, AttributeError):
            pass

    return dicom_series

def write_data(outname, files):
    f = dicom.read_file(files[0])
    array_size = (
            f.pixel_array.shape[0],
            f.pixel_array.shape[1],
            len(files))
    data = numpy.zeros(array_size, dtype = f.pixel_array.dtype)
    for fname in files:
        dicom_file = dicom.read_file(fname)
        assert(dicom_file.pixel_array.shape[0] == array_size[0])
        assert(dicom_file.pixel_array.shape[1] == array_size[0])
        instance_n = int(dicom_file.InstanceNumber) - 1
        data[:, :, instance_n] = dicom_file.pixel_array
    numpy.save(outname, data)

def write_info(outname, files):
    f = dicom.read_file(files[0])
    array_size = (
            f.pixel_array.shape[0],
            f.pixel_array.shape[1],
            len(files))
    phys_size = numpy.array([
            float(f.PixelSpacing[0]),
            float(f.PixelSpacing[1]),
            float(f.SliceThickness)])
    numpy.save(outname, phys_size)




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description =
    "Convert dicom datasets into npy data format.")
    parser.add_argument(
            "input",
            help = 'The path to a dicom study to be converted. %(prog)s will search recursively for dicom series.')
    parser.add_argument(
            "-s", "--series",
            default = None,
            type = int,
            help = "The series to convert.")
    parser.add_argument(
            "-o", "--output",
            default = None,
            help = "Output path.")

    args = parser.parse_args()

    print("Collecting files in {}".format(args.input))
    dicom_files = collect_files(args.input)
    print("Found {} files.".format(len(dicom_files)))
    print("Filtering by series.")
    dicom_series = filter_dicom(dicom_files)
    print("Found {} series.".format(len(dicom_series)))
    for k, v in dicom_series.items():
        print("Series {} size: {}".format(k, len(v)))

    if(args.output):
        series_to_convert = dicom_series.keys()
        if(args.series):
            if(args.series not in series_to_convert):
                print("Error: Invalid series number: {}".format(args.series))
                sys.exit(1)
            series_to_convert = [args.series]
        print("Creating directory {}".format(args.output))
        os.makedirs(args.output)
        for series in series_to_convert:
            fname = "{}.npy".format(series)
            out_name = os.path.join(args.output, fname)
            print("Generating {}".format(out_name))
            write_data(out_name, dicom_series[series])

            info_name = "{}.info.npy".format(series)
            info_out = os.path.join(args.output, info_name)
            print("Generating {}".format(info_out))
            write_info(info_out, dicom_series[series])
